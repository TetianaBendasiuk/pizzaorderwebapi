﻿using Microsoft.AspNetCore.Mvc;
using PizzaWebApi.Models;
using PizzaWebApi.Services;
using System.Collections.Generic;

namespace PizzaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        #region ClassSettings

        private readonly IService<Pizza> _pizzaService;

        public MenuController(IService<Pizza> pizzaService)
        {
            _pizzaService = pizzaService;
        }

        #endregion

        [HttpGet]
        public ActionResult<List<Pizza>> Get()
        {
            var pizzas = _pizzaService.Get();

            if (pizzas is null)
                return NotFound();

            return pizzas;
        }

        [HttpPost]
        public ActionResult<Pizza> Post(Pizza pizza)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _pizzaService.Insert(pizza);
            return Ok(); //Created 201
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var pizza = _pizzaService.Get(id);

            if (pizza == null)
                return NotFound();

            _pizzaService.Delete(pizza.Id);
            return NoContent();
        }
    }
}
