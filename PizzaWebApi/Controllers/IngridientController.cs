﻿using Microsoft.AspNetCore.Mvc;
using PizzaWebApi.Models;
using PizzaWebApi.Services;
using System.Collections.Generic;

namespace PizzaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngridientController : ControllerBase
    {
        #region ClassSettings
        private readonly IService<Ingridient> _indridientService;

        public IngridientController(IService<Ingridient> indridientService)
        {
            _indridientService = indridientService;
        }

        #endregion

        [HttpGet]
        public ActionResult<List<Ingridient>> Get()
        {
            var ingridients = _indridientService.Get();

            if(ingridients is null)
                return NotFound();

            return ingridients;
        }

        [HttpGet("{id:length(24)}", Name = "GetIngridient")]
        public ActionResult<Ingridient> Get(string id)
        {
            var ingridient = _indridientService.Get(id);

            if (ingridient is null)
                return NotFound();

            return ingridient;
        }

        [HttpPost]
        public ActionResult<Ingridient> Post(Ingridient ingridient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _indridientService.Insert(ingridient);
            return Ok(); //Created 201
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Ingridient ingridientUpdate)
        {
            var ingridient = _indridientService.Get(id);

            if (ingridient is null)
                return NotFound();

            _indridientService.Update(id, ingridientUpdate);
            return Ok();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var ingridient = _indridientService.Get(id);

            if (ingridient == null)
                return NotFound();

            _indridientService.Delete(ingridient.Id);
            return NoContent();
        }
    }
}
