﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using PizzaWebApi.Services;
using PizzaWebApi.Models;

namespace PizzaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PizzaController : ControllerBase
    {
        #region ClassSettings

        private readonly IService<Pizza> _pizzaService;
        private readonly IService<Ingridient> _ingridientService;
        private readonly IService<CustomPizza> _customPizzaService;
        private readonly IFriedgeService _friedgeService;

        public PizzaController(IService<Pizza> pizzaService, 
            IService<Ingridient> ingridientService,
            IService<CustomPizza> customPizzaService,
            IFriedgeService friedgeService)
        {
            _pizzaService = pizzaService;
            _ingridientService = ingridientService;
            _customPizzaService = customPizzaService;
            _friedgeService = friedgeService;
        }

        #endregion

        [HttpGet("{id:length(24)}", Name = "GetPizza")]
        public ActionResult<Pizza> Get(string id)
        {
            var pizza = _pizzaService.Get(id);

            if (pizza is null)
                return NotFound(id);

            _friedgeService.CookingProcess(pizza);

            return Ok(pizza);
        }

        [HttpPost]
        public ActionResult Post([FromBody] CustomPizza pizza)
        {
            var ingridientsCount = pizza.IngridientId.Count;

            if (ingridientsCount < 3)
                return BadRequest("Not enough ingridients for pizza!");

            List<string> NoIndridients = new List<string>();

            foreach (var item in pizza.IngridientId)
            {
                Ingridient ingridient = _ingridientService.Get(item);

                if (ingridient.AmountInStorage <= 0)
                {
                    NoIndridients.Add(ingridient.Name);
                }
                else
                {
                    pizza.Price += ingridient.Price;
                }
            }
            _friedgeService.CookingProcess(pizza);

            if (NoIndridients.Count > 0)
                return BadRequest(new { NoIngridients = NoIndridients });

            _customPizzaService.Insert(pizza);

            return Ok(pizza); //Created 201
        }

        [HttpPut("{id:length(24)}")]
        public ActionResult Put(string id, Pizza pizzaUpdate)
        {
            var pizza = _pizzaService.Get(id);

            if (pizzaUpdate == null)
                return BadRequest();

            if (pizza is null)
            {
                _pizzaService.Insert(pizzaUpdate);
                return Created("GetPizza", pizzaUpdate);
            }
            else
            {
                _pizzaService.Update(id, pizzaUpdate);
                return Ok();
            }
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var pizza = _pizzaService.Get(id);

            if (pizza == null)
                return NotFound();

            _pizzaService.Delete(pizza.Id);
            return NoContent();
        }
    }
}