﻿using PizzaWebApi.Models;
using System.Collections.Generic;

namespace PizzaWebApi.Services
{
    public interface IService<T> where T : IBaseEntity
    {
        List<T> Get();
        T Get(string id);
        void Insert(T item);
        void Update(string id, T itemToUpdate);
        void Delete(string id);
    }
}
