﻿using PizzaWebApi.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaWebApi.Services
{
    public interface IFriedgeService
    {
        void CookingProcess(BasePizza pizza);
        void GetIngridients(IList<string> ingridientsIds);
    }
}
