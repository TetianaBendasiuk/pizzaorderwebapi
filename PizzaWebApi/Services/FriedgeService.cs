﻿using PizzaWebApi.Models.Entities;
using System.Collections.Generic;
using System.Threading;

namespace PizzaWebApi.Services
{
    public class FriedgeService : IFriedgeService
    {
        public void CookingProcess(BasePizza pizza)
        {
            GetIngridients(pizza.IngridientId);
            Thread.Sleep(10000);
        }

        public void GetIngridients(IList<string> ingridientsIds)
        {
            foreach (var item in ingridientsIds)
            {
                Thread.Sleep(3000);
            }
        }
    }
}
