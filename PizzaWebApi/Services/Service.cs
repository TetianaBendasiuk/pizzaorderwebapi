﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PizzaWebApi.Models;
using PizzaWebApi.Repositories;

namespace PizzaWebApi.Services
{
    public class Service<T> : IService<T> where T : IBaseEntity
    {
        #region ClassSettings

        private readonly IRepository<T> _repository;

        public Service(IRepository<T> repository)
        {
            _repository = repository;
        }

        #endregion

        public List<T> Get()
        {
             var result = _repository.Get();
            return result;
        }

        public T Get(string id)
        {
            var res = _repository.GetByIdAsync(id);
            return res.Result;
        }

        public void Insert(T item)
        {
            _repository.InsertOneAsync(item);
        }

        public void Update(string id, T itemToUpdate)
        {
            _repository.ReplaceOneAsync(id, itemToUpdate);
        }

        public void Delete(string id)
        {
            _repository.DeleteAsync(id);
        }

        public void Delete(T item)
        {
            _repository.DeleteAsync(item);
        }
    }
}