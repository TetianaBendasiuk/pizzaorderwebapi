﻿const uri = 'api/pizza';
let pizzas = [];

async function GetPizzas() {

    //const response = await fetch(uri);
    const response = await fetch("api/menu", {
        method: "GET",
        headers: { "Accept": "application/json" }
    });

    if (response.ok === true) {
        const pizzas = await response.json();

        let rows = document.querySelector("tbody");

        pizzas.forEach(pizza => {
            rows.append(row(pizza));
        });
    }
}

//async function GetIngridientName(id) {

//    const response = await fetch("api/ingridient/" + id, {
//        method: "GET",
//        headers: { "Accept": "application/json" }
//    });

//    if (response.ok === true) {
//        const ingr = await response.json();

//        const name = ingr.name;
//        console.log(name);
//        return name;
//    }
//}

function row(pizza) {

    const tr = document.createElement("tr");
    tr.setAttribute("id", pizza.id);

    const nameTd = document.createElement("td");
    nameTd.append(pizza.name);
    tr.append(nameTd);

    const priceTd = document.createElement("td");
    priceTd.append(pizza.price);
    tr.append(priceTd);

    return tr;
}