﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PizzaWebApi.Database;
using PizzaWebApi.Models.Entities;

namespace PizzaWebApi.Models
{
    [BsonCollection("Menu")]
    public class Pizza : BasePizza
    {
        [BsonElement("Menu_id")]
        [BsonRepresentation(BsonType.Int32)]
        public int MenuNumber { get; set; }

        [BsonRequired]
        [Required(ErrorMessage = "Please, write a name")]
        public string Name { get; set; }

        public Pizza(List<string> ids) : base(ids)
        {}

        public Pizza() : base()
        {
        }
    }
}
