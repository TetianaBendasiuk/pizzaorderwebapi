﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PizzaWebApi.Models
{
    public interface IBaseEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        string Id { get; set; }
    }
}
