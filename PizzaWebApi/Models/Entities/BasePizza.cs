﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PizzaWebApi.Models.Entities
{
    public class BasePizza : IBaseEntity
    {
        public string Id { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Price { get; set; }

        [BsonElement("Ingridient_id")]
        [BsonRepresentation(BsonType.ObjectId)]
        public IList<string> IngridientId { get; set; }

        public BasePizza()
        {}

        public BasePizza(IList<string> ingridients)
        {
            IngridientId = ingridients;
        }
    }
}
