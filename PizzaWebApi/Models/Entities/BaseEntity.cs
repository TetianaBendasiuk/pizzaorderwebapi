﻿namespace PizzaWebApi.Models
{
    public class BaseEntity : IBaseEntity
    {
        public string Id { get; set; }
    }
}