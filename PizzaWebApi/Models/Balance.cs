﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PizzaWebApi.Database;

namespace PizzaWebApi.Models
{
    [BsonCollection("Balance")]
    public class Balance: BaseEntity
    {
        [BsonElement("Income")]
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal PizzeriaBalance { get; set; }
    }
}
