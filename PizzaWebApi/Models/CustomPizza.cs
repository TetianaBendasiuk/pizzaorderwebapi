﻿using PizzaWebApi.Database;
using PizzaWebApi.Models.Entities;
using System.Collections.Generic;

namespace PizzaWebApi.Models
{
    [BsonCollection("PizzasHistory")]
    public class CustomPizza : BasePizza
    {
        public CustomPizza() : base()
        {}

        public CustomPizza(IList<string> ids) : base(ids)
        { }

        public CustomPizza(IList<string> ids, decimal price) : base(ids)
        {
            Price = price;
        }
    }
}
