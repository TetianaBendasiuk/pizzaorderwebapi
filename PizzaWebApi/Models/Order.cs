﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PizzaWebApi.Database;
using PizzaWebApi.Models.Entities;

namespace PizzaWebApi.Models
{
    [BsonCollection("Orders")]
    public class Order: BaseEntity
    {
        [BsonElement("Items")]
        public IList<BasePizza> Pizzas { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Price { get; set; }

        [BsonRepresentation(BsonType.Boolean)]
        public bool Paid { get; set; } = false;

    }
}
