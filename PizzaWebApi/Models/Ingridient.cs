﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PizzaWebApi.Database;

namespace PizzaWebApi.Models
{
    [BsonCollection("Ingridients")]
    public class Ingridient : BaseEntity
    {
        [BsonRequired]
        [Required(ErrorMessage = "Please, write a name")]
        public string Name { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Price { get; set; }

        [BsonRepresentation(BsonType.Decimal128)]
        public decimal AmountInStorage{ get; set; }
    }
}
