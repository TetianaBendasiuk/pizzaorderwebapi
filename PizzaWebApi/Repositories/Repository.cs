﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.IdGenerators;
using PizzaWebApi.Models;
using PizzaWebApi.Database;

namespace PizzaWebApi.Repositories
{
    public class Repository<T> : IRepository<T> where T : IBaseEntity
    {
        #region ClassSettings

        private readonly IMongoCollection<T> _mongoCollection;

        public Repository(IMongoDbSettings settings)
        {
            var db = new MongoClient(settings.ConnectionString).GetDatabase(settings.DatabaseName);
            _mongoCollection = db.GetCollection<T>(GetCollectionName(typeof(T)));
        }

        private string GetCollectionName(Type documentType)
        {
            return ((BsonCollectionAttribute)documentType
                .GetCustomAttributes(typeof(BsonCollectionAttribute), true)
                .FirstOrDefault())?
                .CollectionName;
        }

        #endregion

        public List<T> Get()
        {
            return _mongoCollection.Find(item => true).ToList<T>(); ;
        }

        public Task<List<T>> GetAsync()
        {
            return Task.Run(() => _mongoCollection.Find(item => true).ToListAsync());
        }

        public Task<T> GetByIdAsync(string id)
        {
            return Task.Run(() => _mongoCollection.Find(item => item.Id == id).FirstOrDefaultAsync());
        }

        public Task InsertOneAsync(T item)
        {
            return Task.Run(() =>
            {
                item.Id = new BsonObjectIdGenerator().GenerateId(_mongoCollection, item).ToString();
                _mongoCollection.InsertOne(item);
            });
        }

        public void ReplaceOneAsync(string id, T item)
        {
            Task.Run(() => _mongoCollection.ReplaceOneAsync(element => id == element.Id, item));
        }

        public Task DeleteAsync(string id)
        {
            return Task.Run(() => _mongoCollection.DeleteOneAsync(el => el.Id == id));
        }

        public Task DeleteAsync(T item)
        {
            return Task.Run(() => _mongoCollection.DeleteOneAsync(el => el.Id == item.Id));
        }
    }
}
