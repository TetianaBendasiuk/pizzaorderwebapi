﻿using PizzaWebApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PizzaWebApi.Repositories
{
    public interface IRepository<T> where T : IBaseEntity
    {
        List<T> Get();
        Task<List<T>> GetAsync();
        Task<T> GetByIdAsync(string id);
        Task InsertOneAsync(T item);
        void ReplaceOneAsync(string id, T item);
        Task DeleteAsync(string id);
        Task DeleteAsync(T item);
    }
}